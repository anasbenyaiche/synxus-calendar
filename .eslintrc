{
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "sourceType": "module",
    "allowImportExportEverywhere": false,
    "codeFrame": false,
    "ecmaVersion": 2018
  },
  "root": true,
  "extends": [
    "airbnb",
    "prettier",
    "prettier/babel",
    "plugin:css-modules/recommended",
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended"
  ],
  "env": {
    "browser": true,
    "jest": true,
    "es6": true
  },
  "plugins": [
    "prettier",
    "prefer-arrow",
    "css-modules",
    "@typescript-eslint",
    "import",
    "react-hooks"
  ],
  "rules": {
    "prefer-arrow/prefer-arrow-functions": [
      "warn",
      {
        "disallowPrototype": true,
        "singleReturnOnly": false,
        "classPropertiesAllowed": false
      }
    ],
    "react-hooks/exhaustive-deps": "warn",
    "jsx-a11y/label-has-associated-control": "off",
    "react/prop-types": "off",
    "no-extra-parens": "off",
    "no-return-await": "off",
    "no-await-in-loop": "off",
    "no-plusplus": "off",
    "no-underscore-dangle": "off",
    "space-before-function-paren": "off",
    "prefer-arrow-callback": ["error", { "allowNamedFunctions": false }],
    "prefer-const": "error",
    "@typescript-eslint/explicit-module-boundary-types": "off",
    "prefer-destructuring": [
      "error",
      {
        "VariableDeclarator": {
          "array": false,
          "object": true
        },
        "AssignmentExpression": {
          "array": true,
          "object": true
        }
      },
      {
        "enforceForRenamedProperties": false
      }
    ],
    "@typescript-eslint/interface-name-prefix": "off",
    "no-console": "warn",
    "react/jsx-filename-extension": "off",
    "no-const-assign": "error",
    "import/extensions": "off",
    "no-var": "error",
    "no-param-reassign": "off",
    "no-unused-expressions": "off",
    "@typescript-eslint/ban-types": [
      "error",
      {
        "types": {
          // add a custom message to help explain why not to use it
          "Foo": "Don't use Far because it is unsafe",

          // add a custom message, AND tell the plugin how to fix it
          "String": {
            "message": "Use string instead",
            "fixWith": "string"
          },

          "{}": {
            "message": "Use object instead",
            "fixWith": "object"
          },
          "unkown": {
            "message": "unkown is not allowed"
          },
          "never": {
            "message": "never is not allowed"
          },
          "any": {
            "message": "any is not allowed"
          }
        }
      }
    ],

    "arrow-parens": ["error", "as-needed"],
    "max-len": ["error", { "code": 145 }],
    "react/prefer-stateless-function": "error",
    "prefer-promise-reject-errors": ["off"],
    "react/jsx-props-no-spreading": "off",
    "no-return-assign": ["off"],
    "import/no-cycle": [0, { "maxDepth": 3 }],
    "import/imports-first": 0,
    "max-lines": ["error", 145],
    "import/newline-after-import": 0,
    "import/no-dynamic-require": 0,
    "import/no-extraneous-dependencies": 0,
    "import/no-named-as-default": 0,
    "import/no-unresolved": 2,
    "import/no-webpack-loader-syntax": 0,
    "import/prefer-default-export": 0,
    "no-unused-vars": "warn",
    "no-use-before-define": 0,
    "prefer-template": 2,
    "require-yield": 0,
    "arrow-spacing": "error"
  },
  "settings": {
    "import/parsers": {
      "@typescript-eslint/parser": [".ts", ".tsx"]
    },
    "import/resolver": {
      "typescript": {
        "directory": "./tsconfig.json" // always try to resolve types under `<roo/>@types` directory even it doesn't contain any source code, like `@types/unist`
      }
    }
  }
}
