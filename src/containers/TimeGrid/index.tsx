import SchedulerUnit from 'components/SchedulerUnit';
import { hoursPerDay } from 'constant/hoursPerDay';
import { BookingInterface } from 'constant/Models/BookingInterface';
import { TimeSlotBookingInterface } from 'constant/Models/TimeSlotBookingInterface';
import EventContainer from 'containers/EventContainer/EventContainer';
import { Moment } from 'moment';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '_redux/reducers';
import { BookingTypes } from '_redux/types/bookingTypes';

interface ResourceInterface {
  resourceId: string;
  resourceTitle: string;
}

interface TimeGridInterface {
  resources?: ResourceInterface[];
  date?: Date;
  onSelectBooking?: (booking: BookingInterface) => void;
  onSelectTimeSlot?: (timeSlot: TimeSlotBookingInterface) => void;
}
// Time Grid section where  the resource is mapped following time
const TimeGrid: React.FC<TimeGridInterface> = ({
  onSelectBooking,
  onSelectTimeSlot,
}) => {
  // Get the resource & date & the booking
  const { resources } = useSelector((state: RootState) => state.resource);
  const { date } = useSelector((state: RootState) => state.time);
  const { bookings } = useSelector((state: RootState) => state.booking);
  // calling the action to select a time slot
  const selectBookingTimeSlotDispatch = useDispatch();

  // Check if there is a booking on Date and the Resource
  const isBookingExist = (
    bookingList: BookingInterface[],
    targetResource: ResourceInterface,
    selectedDate: Moment,
  ) => {
    const targetBooking = !bookingList.find(
      bookingElement =>
        bookingElement.resourceId === targetResource.resourceId &&
        bookingElement.bookingDate === selectedDate.format('YYYY-MM-DD'),
    );
    let isBooking = false;
    if (targetBooking) {
      isBooking = true;
    }
    return isBooking;
  };
  const handleSelectTimeSlot = (
    resource: ResourceInterface,
    selectedDate: Moment,
  ) => {
    // const starting = (e.clientY / 730) * 100;

    const timeSlot = {
      bookingDate: selectedDate.format('YYYY-MM-DD'),
      resourceId: resource.resourceId,
      start: '',
      end: '',
    };

    selectBookingTimeSlotDispatch({
      type: BookingTypes.SELECT_A_TIME_SLOT_BOOKING,
      payload: timeSlot,
    });
    onSelectTimeSlot?.(timeSlot);
  };

  const timeColumn = hoursPerDay.map(timeInHour => (
    <SchedulerUnit key={timeInHour}>{timeInHour}</SchedulerUnit>
  ));

  const resourceBlocks = resources?.map(resourceElement => (
    <div
      key={resourceElement.resourceId}
      onMouseDown={() =>
        isBookingExist(bookings, resourceElement, date) &&
        handleSelectTimeSlot(resourceElement, date)}
      className="calendar-tool-event-container"
      role="button"
      tabIndex={0}
    >
      <SchedulerUnit isSelectable resource={resourceElement}>
        {resourceElement.resourceTitle}
      </SchedulerUnit>
      <EventContainer
        resourceId={resourceElement.resourceId}
        {...{ onSelectBooking }}
      />
    </div>
  ));
  return (
    <div className="time-grid-container">
      <div className="calendar-time-container">
        <SchedulerUnit> </SchedulerUnit>
        {timeColumn}
      </div>
      {resourceBlocks}
    </div>
  );
};

export default TimeGrid;
