import EventUnit from 'components/EventUnit';
import { BookingInterface } from 'constant/Models/BookingInterface';
import { convertTimeSlotFormat } from 'helpers/convertTimeSlot';
import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '_redux/reducers';

interface EventContainerInterface {
  resourceId: string;
  date?: Date;
  onSelectBooking ?: (booking : BookingInterface) => void
}

const EventContainer: React.FC<EventContainerInterface> = ({ resourceId , onSelectBooking }) => {
  const { bookings } = useSelector((state: RootState) => state.booking);
  const { date } = useSelector((state: RootState) => state.time);
  const eventDate = date.format('YYYY-MM-DD');
  const selectedbookings = bookings?.filter(
    selectedbooking =>
      selectedbooking.resourceId === resourceId &&
      selectedbooking.bookingDate === eventDate,
  );

  return (
    <div>
      {selectedbookings?.map(bookingElement => (
        <div key={bookingElement.bookingId}>
          <EventUnit
            {...{onSelectBooking}}
            start={bookingElement.start}
            booking={bookingElement}
            end={bookingElement.end}
          >
            {`${convertTimeSlotFormat(
              parseInt(bookingElement.start, 10),
            )} - ${convertTimeSlotFormat(parseInt(bookingElement.end, 10))}`}
          </EventUnit>
        </div>
      ))}
    </div>
  );
};
export default EventContainer;
