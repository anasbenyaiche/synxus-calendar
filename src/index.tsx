import './index.css';
import * as React from 'react';

import { Provider } from 'react-redux';

import store from '_redux/store';
import Scheduler from 'pages/Calendar';
import { resourceMap } from 'constant/resource';
import { bookingsMap } from 'constant/bookings';
import { ResourceInterFace } from 'constant/Models/ResourceInterface';
import { BookingInterface } from 'constant/Models/BookingInterface';
import { TimeSlotBookingInterface } from 'constant/Models/TimeSlotBookingInterface';

interface ISchedulerinterface {
  resources: ResourceInterFace[];
  bookings: BookingInterface[];
  onSelectBooking?: (booking: BookingInterface) => void;
  onSelectTimeSlot?: (timeSlot: TimeSlotBookingInterface) => void;
}

const EdonecScheduler: React.FC<ISchedulerinterface> = ({
  resources = resourceMap,
  bookings = bookingsMap,
  onSelectBooking,
  onSelectTimeSlot,
}) => (
  <Provider store={store}>
    <Scheduler
      resources={resources}
      bookings={bookings}
      {...{ onSelectBooking, onSelectTimeSlot }}
    />
  </Provider>
);

export default EdonecScheduler;
