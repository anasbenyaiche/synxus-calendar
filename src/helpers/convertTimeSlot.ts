export const convertTimeSlotFormat = (timeValue:number) => {
  
    let amOrPm = "A.M";
    let toHours = Math.floor(timeValue / 3600);
  
    const toMinutes = Math.floor((timeValue % 3600) / 60);
  
  
  
    if (toHours > 12) {
      amOrPm = "P.M";
      toHours -= 12;
    }
    if (toHours === 24) {
      amOrPm = "A.M";
      toHours = 0;
    }
    const hoursValue = toHours < 10 ? `0${toHours.toString()}` : toHours.toString();
    const minutesValue = toMinutes < 10 ? `0${toMinutes.toString()}` : toMinutes.toString();
    
  
    const hoursMinutesSecondsFormat = `${hoursValue}:${minutesValue} ${amOrPm}`;
  
    return hoursMinutesSecondsFormat;
  };
  