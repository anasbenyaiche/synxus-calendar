export interface ISelectorOption {
  label: string;
  value: unknown;
}
