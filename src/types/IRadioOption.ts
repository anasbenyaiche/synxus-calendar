import { ISelectorOption } from './ISelectorOption';

export interface IRadioOption extends ISelectorOption {
  value: string;
}
