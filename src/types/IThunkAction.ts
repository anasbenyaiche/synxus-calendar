import { ThunkAction } from 'redux-thunk';
import { RootState } from '_redux/reducers/';
import { IReduxAction } from './IReduxAction';
export type IThunkAction<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  IReduxAction
>;
