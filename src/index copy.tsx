import './index.scss';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as serviceWorker from 'serviceWorker';

import { Provider } from 'react-redux';

import store from '_redux/store';
import Scheduler from 'pages/Calendar';
import { resourceMap } from 'constant/resource';
import { bookingsMap } from 'constant/bookings';
import { ResourceInterFace } from 'constant/Models/ResourceInterface';
import { BookingInterface } from 'constant/Models/BookingInterface';

interface Schedulerinterface {
  resources: ResourceInterFace[];
  bookings: BookingInterface[];
}

ReactDOM.render(
  <Provider store={store}>
    <Scheduler resources={resourceMap} bookings={bookingsMap} />
  </Provider>,

  document.getElementById('root'),
);

serviceWorker.unregister();
