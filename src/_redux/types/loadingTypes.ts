export enum LoadingTypes {
  LOADING_START = 'LOADING_START',
  LOADING_FINISHED = 'LOADING_FINISHED',
}
