import { IReduxAction } from 'types/IReduxAction';
import moment, { Moment } from 'moment';
import { TimeEventsTypes } from '_redux/types/timeEventsTypes';

const initialState = {
  date: moment(),
  daysCounter: 0,
};

export interface TimeState {
  date: Moment;
  daysCounter: number;
}

export default (
  state: TimeState = initialState,
  action: IReduxAction,
): TimeState => {
  const { date, daysCounter } = state;
  const { type, payload } = action;
  switch (type) {
    case TimeEventsTypes.ADD_DAY:
      return {
        date: moment().add(daysCounter + 1, 'days'),
        daysCounter: daysCounter + 1,
      };
    case TimeEventsTypes.SUBSTRACT_DAY:
      return {
        date: moment().subtract(-daysCounter + 1, 'days'),
        daysCounter: daysCounter - 1,
      };
    case TimeEventsTypes.RESET_TODAY:
      return {
        daysCounter: 0,
        date: moment(),
      };
    case TimeEventsTypes.NAVIGATE_TO_DAY:
      if (payload.valueOf() > date.valueOf()) {
        return {
          daysCounter: daysCounter + moment(payload).diff(moment(date), 'days'),
          date: payload,
        };
      }

      return {
        daysCounter: daysCounter - moment(date).diff(moment(payload), 'days'),
        date: payload,
      };

    default:
      return state;
  }
};
