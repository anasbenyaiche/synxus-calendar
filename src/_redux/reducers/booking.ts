import { BookingInterface } from 'constant/Models/BookingInterface';
import { TimeSlotBookingInterface } from 'constant/Models/TimeSlotBookingInterface';
import { IReduxAction } from 'types/IReduxAction';
import { BookingTypes } from '_redux/types/bookingTypes';

const initialState = {
  bookings: [],
  selectedBooking: {
    bookingId: '',
    start: '',
    end: '',
    bookingDate: '',
    resourceId: '',
  },
  selectedBookingTimeSlot: {
    bookingDate: '',
    resourceId: '',
    start: '',
    end: '',
  },
};

export interface bookingState {
  bookings: BookingInterface[];
  selectedBooking: BookingInterface;
  selectedBookingTimeSlot: TimeSlotBookingInterface;
}

export default (
  state: bookingState = initialState,
  action: IReduxAction,
): bookingState => {
  // const { bookings } = state;
  const { type, payload } = action;
  switch (type) {
    case BookingTypes.GET_BOOKINGS:
      return {
        ...state,
        bookings: payload,
      };
    case BookingTypes.SELECT_A_BOOKING:
      return {
        ...state,
        selectedBooking: payload,
      };
    case BookingTypes.SELECT_A_TIME_SLOT_BOOKING:
      return { ...state, selectedBookingTimeSlot: payload };
    default:
      return state;
  }
};
