import { IReduxAction } from 'types/IReduxAction';
import { StyleOptionTypes } from '_redux/types/StyleOptionTypes';

const initialState = {
  eventUnitClass: '',
  eventCotnainerClass: '',
  schedularUnitClass: '',
  timeGridClass: '',
  toolbarClass: '',
  schedularContainerClass: '',
};

export interface optionStyleState {
  eventUnitClass: string;
  eventCotnainerClass: string;
  schedularUnitClass: string;
  timeGridClass: string;
  toolbarClass: string;
  schedularContainerClass: string;
}

export default (
  state: optionStyleState = initialState,
  action: IReduxAction,
): optionStyleState => {
  // const { resources } = state;
  const { type, payload } = action;
  switch (type) {
    case StyleOptionTypes.GET_EVENT_UNIT_CLASS:
      return {
        ...state,
        eventUnitClass: payload,
      };
    case StyleOptionTypes.GET_EVENT_CONTAINER_CLASS:
      return {
        ...state,
        eventCotnainerClass: payload,
      };
    case StyleOptionTypes.GET_SCHEDULAR_UNIT_CLASS:
      return {
        ...state,
        schedularUnitClass: payload,
      };
    case StyleOptionTypes.GET_SCHEDULAR_CONTAINER_CLASS:
      return {
        ...state,
        schedularContainerClass: payload,
      };
    case StyleOptionTypes.GET_TIME_GRID_CLASS:
      return {
        ...state,
        timeGridClass: payload,
      };
    case StyleOptionTypes.GET_TOOLBAR_CLASS:
      return {
        ...state,
        toolbarClass: payload,
      };
    default:
      return state;
  }
};
