import { combineReducers } from 'redux';

import resource from './resource';
import time from './time';
import booking from './booking';
import styleOption from './styleOption';

const rootReducer = combineReducers({
  resource,
  time,
  booking,
  styleOption,
});
export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;
