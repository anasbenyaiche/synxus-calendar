import { ResourceInterFace } from 'constant/Models/ResourceInterface';

import { IReduxAction } from 'types/IReduxAction';
import { ResourceTypes } from '_redux/types/resourceType';

const initialState = {
  resources: [],
  selectedResource: { resourceId: '', resourceTitle: '' },
};

export interface resourceState {
  resources: ResourceInterFace[];
  selectedResource: ResourceInterFace;
}

export default (
  state: resourceState = initialState,
  action: IReduxAction,
): resourceState => {
  // const { resources } = state;
  const { type, payload } = action;
  switch (type) {
    case ResourceTypes.GET_RESOURCE:
      return { ...state, resources: payload };
    case ResourceTypes.SELECT_A_RESOURCE:
      return { ...state, selectedResource: payload };
    default:
      return state;
  }
};
