import { BookingInterface } from 'constant/Models/BookingInterface'
import React from 'react'


export const CalendarContext = React.createContext<{
    selectedBooking ?: BookingInterface,
    setSelectedBooking : (booking : BookingInterface) => void
// eslint-disable-next-line @typescript-eslint/no-empty-function
}>({setSelectedBooking :  () => {} })