export interface TimeSlotBookingInterface {
    bookingDate:string,
    resourceId:string,
    start:string,
    end:string,
}