export interface BookingInterface {

    bookingId: string,
    start: string,
    end: string,
    bookingDate: string,
    resourceId: string,

}
