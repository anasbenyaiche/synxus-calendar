export const resourceMap = [
  {
    resourceId: '1',
    resourceTitle: 'Test Space',
  },
  {
    resourceId: '2',
    resourceTitle: 'Tool',
  },
  {
    resourceId: '3',
    resourceTitle: 'Printer 3D',
  },
  { resourceId: '4', resourceTitle: 'Hummer' },
  { resourceId: '5', resourceTitle: 'Laser Saw' },
  {
    resourceId: '6',
    resourceTitle: 'Crucher',
  },
  { resourceId: '7', resourceTitle: 'Cutter' },
  {
    resourceId: '8',
    resourceTitle: 'Vortex',
  },
  {
    resourceId: '10',
    resourceTitle: 'Hummer',
  },
  {
    resourceId: '11',
    resourceTitle: 'Laser Saw',
  },
  { resourceId: '12', resourceTitle: 'Crucher' },
  { resourceId: '13', resourceTitle: 'Cutter' },
  { resourceId: '14', resourceTitle: 'Vortex' },
];
