import React, { useEffect } from 'react';

import TimeGrid from 'containers/TimeGrid';
import Toolbar from 'components/Toolbar';
import { useDispatch, useSelector } from 'react-redux';
import { BookingTypes } from '_redux/types/bookingTypes';
import { ResourceTypes } from '_redux/types/resourceType';
import { BookingInterface } from 'constant/Models/BookingInterface';
import { ResourceInterFace } from 'constant/Models/ResourceInterface';
import { RootState } from '_redux/reducers';
import { TimeSlotBookingInterface } from 'constant/Models/TimeSlotBookingInterface';

interface SchedulerInterface {
  bookings?: BookingInterface[];
  resources?: ResourceInterFace[];
  onSelectBooking?: (booking: BookingInterface) => void;
  onSelectTimeSlot?: (timeSlot: TimeSlotBookingInterface) => void;
}

const Scheduler: React.FC<SchedulerInterface> = ({
  bookings,
  resources,
  onSelectBooking,
  onSelectTimeSlot,
}) => {
  const resourceDispatch = useDispatch();
  const bookingDispatch = useDispatch();

  // get Resource and booking
  useEffect(() => {
    resourceDispatch({ type: ResourceTypes.GET_RESOURCE, payload: resources });
    bookingDispatch({ type: BookingTypes.GET_BOOKINGS, payload: bookings });
  }, [bookingDispatch, bookings, resourceDispatch, resources]);

  const { schedularContainerClass } = useSelector(
    (state: RootState) => state.styleOption,
  );

  return (
    <div className={`calendar-container ${schedularContainerClass}`}>
      <Toolbar />
      <TimeGrid {...{ onSelectBooking,onSelectTimeSlot }}  />
    </div>
  );
};

export default Scheduler;
