import { ResourceInterFace } from 'constant/Models/ResourceInterface';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '_redux/reducers';
import { ResourceTypes } from '_redux/types/resourceType';

interface SchedulerUnitInterface {
  isSelectable?: boolean;
  children?: string;
  resource?: ResourceInterFace;
  // onSelectRessource?: (resource: string) => void;
}

const SchedulerUnit: React.FC<SchedulerUnitInterface> = ({
  children,
  // onSelectRessource,
  isSelectable,
  resource,
}) => {
  const { schedularUnitClass } = useSelector(
    (state: RootState) => state.styleOption,
  );

  const selectResourceDispatch = useDispatch();
  const handleSelectResource = () => {
    selectResourceDispatch({
      type: ResourceTypes.SELECT_A_RESOURCE,
      payload: resource,
    });
  };

  const isCompact = false;
  return (
    <div className="scheduler-single-unit-container">
      <div
        className={` scheduler-unit ${schedularUnitClass}
        ${isCompact ? 'scheduler-unit-compact' : ''}
        ${isSelectable ? 'scheduler-unit-selectable' : ''}`}
        onClick={() => {
          isSelectable && handleSelectResource();
        }}
        role="button"
        tabIndex={0}
        onKeyPress={() => {
          isSelectable && handleSelectResource();
        }}
      >
        {children}
      </div>
    </div>
  );
};

export default SchedulerUnit;
