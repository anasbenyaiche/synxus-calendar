import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '_redux/reducers';
import { TimeEventsTypes } from '_redux/types/timeEventsTypes';
import { Moment } from 'moment';
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import { Grid } from '@material-ui/core';
import MomentUtils from '@date-io/moment';

interface dateInterface {
  value?: Moment;
}

const Toolbar: React.FC = () => {
  const { date } = useSelector((state: RootState) => state.time);

  const daysCountDispatch = useDispatch();

  const handleAddDay = () => {
    daysCountDispatch({ type: TimeEventsTypes.ADD_DAY });
  };
  const handleSubstractDay = () => {
    daysCountDispatch({ type: TimeEventsTypes.SUBSTRACT_DAY });
  };

  const handleResetToday = () => {
    daysCountDispatch({ type: TimeEventsTypes.RESET_TODAY });
  };

  const handleDateChange = (newDate: Moment | null) => {
    daysCountDispatch({
      type: TimeEventsTypes.NAVIGATE_TO_DAY,
      payload: newDate,
    });
  };
  return (
    <div className="calendar-toolbar-container">
      <div>
        <h2 className="date-formate-header">
          {date.format('dddd DD MMMM YYYY')}
        </h2>
        <label className="MuiFormLabel-root MuiInputLabel-root customized-label ">
          Select Timeslot to create a booking event
        </label>
      </div>

      <div className="calendar-toolbar-navigation-container">
        <div>
          <MuiPickersUtilsProvider utils={MomentUtils}>
            <Grid container justify="space-around">
              <KeyboardDatePicker
                InputProps={{
                  disabled: true,
                }}
                emptyLabel="2020-09-11"
                disableToolbar
                variant="inline"
                format="DD-MM-YYYY"
                margin="normal"
                id="date-picker-inline"
                label="Select date"
                value={date}
                onChange={handleDateChange}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
              />
            </Grid>
          </MuiPickersUtilsProvider>
        </div>
        <button
          type="button"
          className="calender-toolbar-button "
          onClick={handleResetToday}
        >
          Today
        </button>
        <button
          type="button"
          className="calender-toolbar-button "
          onClick={handleSubstractDay}
        >
          Previous day
        </button>
        <button
          type="button"
          className="calender-toolbar-button "
          onClick={handleAddDay}
        >
          Next day
        </button>
      </div>
    </div>
  );
};
export default Toolbar;
