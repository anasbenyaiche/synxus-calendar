import { BookingInterface } from 'constant/Models/BookingInterface';
import React from 'react';
import { useDispatch } from 'react-redux';

import styled from 'styled-components';
import { BookingTypes } from '_redux/types/bookingTypes';

interface WrapperProps {
  pixelInterval: string;

  pixelStarts: string;
}
// using styled Component so we can configure the height and position of the Event
const EventUnitElement = styled.div<WrapperProps>`
  font-size: 11px;
  left: 3%;
  position: absolute;
  width: 88%;
  height: ${props => props.pixelInterval};
  top: ${props => props.pixelStarts};
  background-color: #56bda2;
  padding: 4px;
  color: white;
  border-radius: 6px;
  box-shadow: 2px 5px 9px 0px rgba(0, 0, 0, 0.75);
  cursor: pointer;
`;
// converting the amount of hours in % of the container
interface EventUnitInterface {
  start: string;
  end: string;
  booking: BookingInterface;
  children: string;

  onSelectBooking?:(booking : BookingInterface)  => void;
}

const convertHoursToPixels = (time: number, startingMargin: number): string => {
  const portion = Math.round((time * 96) / 86400 + startingMargin);
  const result = portion.toString();
  const valueInPixel = `${result}%`;
  return valueInPixel;
};

const EventUnit: React.FC<EventUnitInterface> = ({
  booking,
  start,
  end,
  children,
  onSelectBooking,
}) => {
  const pixelStarts = convertHoursToPixels(parseInt(start, 10), 4);
  const pixelInterval = convertHoursToPixels(
    parseInt(end, 10) - parseInt(start, 10),
    0,
  );
  const selectBookingDispatch = useDispatch();
  const handleSelectBooking = (bookingToSelect: BookingInterface) => {
    selectBookingDispatch({
      type: BookingTypes.SELECT_A_BOOKING,
      payload: bookingToSelect,
    });
    onSelectBooking?.(bookingToSelect)
  };
  return (
    <div
      className="event-unit-box"
      onClick={() => handleSelectBooking(booking)}
      onKeyPress={() => handleSelectBooking(booking)}
      role="button"
      tabIndex={0}
    >
      <EventUnitElement pixelStarts={pixelStarts} pixelInterval={pixelInterval}>
        {children}
      </EventUnitElement>
    </div>
  );
};

export default EventUnit;
